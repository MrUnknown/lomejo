<header id="banner" class="scrollto clearfix" data-enllax-ratio=".5">
    <div id="header" class="nav-collapse">
        <div class="row clearfix">
            <div class="col-1">

                <!--Logo-->
                <div id="logo">

                    <!--Logo that is shown on the banner-->
                    <img src="images/logo.png" id="banner-logo" alt="Landing Page" />
                    <!--End of Banner Logo-->

                    <!--The Logo that is shown on the sticky Navigation Bar-->
                    <img src="images/logo.png" id="navigation-logo" alt="Landing Page" />
                    <!--End of Navigation Logo-->

                </div>
                <!--End of Logo-->

                <!--Main Navigation-->
                <nav id="nav-main">
                    <ul>
                        <li><a href="#banner">About</a></li>
                        <li><a href="#gallery">Puppies</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
                <!--End of Main Navigation-->

                <div id="nav-trigger"><span></span></div>
                <nav id="nav-mobile"></nav>

            </div>
        </div>
    </div>
    <!--End of Header-->

    <!--Banner Content-->
    <div id="banner-content" class="row clearfix">

        <div class="col-38">

            <div class="section-heading">
                <h1>KUSA-REGISTERED BULL TERRIERS</h1>
                <h2>LOMEJO Bull Terrier Kennel is based in Gauteng, South Africa. We take pride in breeding outstanding pure bred Bull Terriers from top breeders bloodlines. We are a Registered member of the Kennel Union of Southern Africa (KUSA) and an accredited KUSA breeder.</h2>
            </div>

        </div>
        <div class="col-61" id="banner-background"></div>

    </div>
</header>
