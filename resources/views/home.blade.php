<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>LOMEJO Bull Terriers</title>

    <meta name="description" content="LOMEJO Bull Terrier Kennel is based in Gauteng, South Africa. We take pride in breeding outstanding pure bred Bull Terriers from top breeders bloodlines.">
    <meta name="author" content="Johan le Roux">

    <meta property="og:title" content="LOMEJO Bull Terriers">
    <meta property="og:description" content="LOMEJO Bull Terrier Kennel is based in Gauteng, South Africa. We take pride in breeding outstanding pure bred Bull Terriers from top breeders bloodlines.">
    <meta property="og:url" content="http://lomejo.co.za/" />
    <meta property="og:site_name" content="LOMEJO Bull Terriers" />
    <meta property="og:type" content="website"/>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" href="favicon.ico" />

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-81174406-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-81174406-1');
    </script>
</head>
<body>
    <!-- PRELOADER -->
    <div id="preloader">
        <div id="status" class="la-ball-triangle-path">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- BANNER -->
        @include('_banner')

        <main id="content">
            @if($current)
            <!-- CURRENT LITTER -->
            <gallery heading="Current Litter" :images="{{ json_encode($current) }}"></gallery>
            @endif

            @if($previous)
            <!-- GALLERY -->
            <gallery heading="Previous Puppies" :images="{{ json_encode($previous) }}"></gallery>
            @endif

            <!-- CONTACT -->
            <contact></contact>
        </main>
    </div>

    <!-- FOOTER -->
    @include('_footer')

    @if($notification)
        <div id="popup" class="modal">
            <div class="modal-content text-center">
                <span class="close">&times;</span>
                <div class="section-heading mb-0 pb-0">
                    <h2 class="section-title mb-0 pb-0">{{ $notification->title }}</h2>
                </div>
                <p>{!! $notification->body !!}</p>
            </div>
        </div>
    @endif

    <script src="{{ mix('js/all.js') }}"></script>
</body>
</html>
