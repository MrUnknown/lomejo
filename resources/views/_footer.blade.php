<footer id="landing-footer" class="clearfix">
    <div class="row clearfix">
        <p id="copyright" class="col-2">LOMEJO &copy; <script>document.write(new Date().getFullYear());</script> Copyright</p>

        <p class="col-2 text-right">Made by <a href="https://johanleroux.me">JOHAN LE ROUX</a></p>
    </div>
</footer>
