@component('mail::message')
# Contact Us

## From: {{ $name }}
### Email: {{ $email }}
### Phone: {{ $phone }}

{{ $body }}

@component('mail::button', ['url' => 'mailto: ' . $email])
Email {{ $name }}
@endcomponent
@endcomponent
