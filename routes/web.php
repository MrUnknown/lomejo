<?php

use App\Litter;
use App\Notification;
use App\Contact;

Route::get('/', function () {
    $current_litter = Litter::with('media')->latest()->where('current', true)->get();
    $previous_litter = Litter::with('media')->latest()->where('current', false)->get();

    $current = [];
    $previous = [];

    foreach ($current_litter as $value) {
        foreach ($value->getMedia('multi') as $media) {
            $current[] = [
                'full' => $media->getFullUrl(), 
                'thumb' => $media->getFullUrl('thumb')
            ];
        }
    }

    foreach ($previous_litter as $value) {
        foreach ($value->getMedia('multi') as $media) {
            $previous[] = [
                'full' => $media->getFullUrl(),
                'thumb' => $media->getFullUrl('thumb')
            ];
        }
    }

    $notification = Notification::where('active', true)->latest()->first();

    return view('home', compact('current', 'previous', 'notification'));
});

Route::post('/contact', function () {
    $validator = Validator::make(request()->all(), [
        'name'    => 'required|max:255',
        'email'   => 'required|max:255|email',
        'phone'   => 'required|max:255',
        'message' => 'required',
    ])->validate();

    Contact::create(request()->all());

    // Process mail
    Mail::send(new App\Mail\ContactPage([
        'request' => request()->all()
    ]));

    return 'MESSAGE HAS BEEN SENT!';
})->name('contact');
