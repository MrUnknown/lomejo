<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactPage extends Mailable
{
  use Queueable, SerializesModels;

  public $body, $name, $phone, $email;

  /**
  * Create a new message instance.
  *
  * @return void
  */
  public function __construct($data)
  {
    $this->body  = $data["request"]["message"];
    $this->name  = $data["request"]["name"];
    $this->phone = $data["request"]["phone"];
    $this->email = $data["request"]["email"];
  }

  /**
  * Build the message.
  *
  * @return $this
  */
  public function build()
  {
    return $this->subject('Contact Page - ' . $this->name)
                ->to(env('MAIL_SEND_ADDRESS'))
                ->markdown('emails.contact');
  }
}
