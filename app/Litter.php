<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;

class Litter extends Model implements HasMedia
{
    use HasMediaTrait;
    
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_FILL, 390, 290)
            ->background('FFFFFF');
    }
    
    public function registerMediaCollections()
    {
        $this->addMediaCollection('single')->useDisk('s3')->singleFile();

        $this->addMediaCollection('multi')->useDisk('s3');
    }
}