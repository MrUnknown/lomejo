let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .scripts([
        'public/js/jquery.1.8.3.min.js',
        'public/js/wow.min.js',
        'public/js/featherlight.min.js',
        'public/js/featherlight.gallery.min.js',
        'public/js/jquery.scrollUp.min.js',
        'public/js/jquery.stickyNavbar.min.js',
        'public/js/jquery.waypoints.min.js',
        'public/js/app.js',
    ], 'public/js/all.js')
    .version();
